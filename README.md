# chrome浏览器插件，用于计算每天距离下班时间还剩余多少
## 如果喜欢的话 点击右上角 `Star` 给我个小星星吧~~~



## 安装地址:

### 安装教程

1. #### 视频演示地址：[bilibili 视频安装演示地址](http://xianbai.me/learn-md/article/syntax/links.html)

2. #### 安装文档： [click my]()

   

## 本次更新说明：

本次更新基本重写底层算法，引入monent.js，jquery，bootstrap3 第三方类库

1.  [√] 可自定义上下班时间
2.  [√] 距离下班剩余时间
3.  [√] 距离周末剩余时间
4.  [√] 距离月底剩余时间
5.  [√] 增加双休，单休 周末类型
6.  [ x ] 增加发工资时间 - 未完成





## 界面效果图

![NDOaP1.png](https://s1.ax1x.com/2020/06/26/NDOaP1.png)
![NDOd8x.png](https://s1.ax1x.com/2020/06/26/NDOd8x.png)
![NDON5R.png](https://s1.ax1x.com/2020/06/26/NDON5R.png)
![NDOta9.png](https://s1.ax1x.com/2020/06/26/NDOta9.png)



### 插件说明
 + 默认上班时间为 9:00
 + 默认下班时间为 18:00
 + 默认周末类型为 双休


### 新版本更新说明
 + 引入了moment js日期框架
 + 优化底层计算日期
 + 增加 自定义上下班时间
 + 增加 周末距离类型



## 结束语

```

Version 2.0
当前版本为2.0，如遇到Bug，请提出Issues。
感谢。
Created By:jim 2020/6/21

===============================================================

Version 1.0
Created By:jim 2020/6/9



```

![visitor badge](https://visitor-badge.glitch.me/badge?page_id=sb.jim.-go-home-.github.2020.06.26)